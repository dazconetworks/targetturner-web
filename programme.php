<?php
require("/var/www/html/common/phpMQTT/phpMQTT.php");

// Start the session
session_start();

// for infinite time of execution 
ini_set('max_execution_time', '0'); 

if(isset($_POST['submit'])) 
{ 
    $node = $time = $direction = $duration = "";
    
    if (empty($_POST["node"])) {
        $nodeErr = "Target is required";
      } else {
        $node = $_POST["node"];
      }
    if (empty($_POST["time"])) {
        $time = "0";
      } else {
        $time = $_POST["time"];
      }
    if (empty($_POST["direction"])) {
        $direction = "0";
      } else {
        $direction = $_POST["direction"];
      }

    if (empty($_POST["duration"])) {
        $duration = "10";
      } else {
        $duration = $_POST["duration"];
      }

      if (empty($_POST["delay"])) {
        $delay = "0";
      } else {
        $delay = $_POST["delay"];
      }

    function sendRequest($msg) {

        //send request to target
        $host = "127.0.0.1";
        $port = 1883;

        //MQTT client id to use for the device. "" will generate a client id automatically
        $mqtt = new phpMQTT($host, $port, "ClientID".rand());

        if ($mqtt->connect(true,NULL)) 
        {
            $mqtt->publish("trigger/targets",$msg, 0);    
            $mqtt->close();
        }
        else
        {
            echo "Fail or time out<br />";
        }

        if ($direction == 1) {
            $directionText = "Anti-Clockwise";
        }
        else
        {
            $directionText = "Clockwise";
        }
        $output = "Target: $node, Direction: $directionText, Time: $time seconds";
    
        $_SESSION['output'] =  $_SESSION['output']+$output;
    }

    function turnTargets($n, $di, $t) {
        if ($di == "0") {
            $di = rand(1, 2);
        }
        if ($t == "0") {
            $t = rand(2,10);
        }

        $msg = "$n,$di,$t,0";
        sendRequest($msg);
    }

    for($x = 0; $x <= (int)$duration; $x++) {
        turnTargets($node, $direction, $time);
        if ($delay == "0") {
            sleep(rand(5,20));
        }
        else
        {
            sleep((int)$delay);
        }
    }
} 

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/jumbotron.css" rel="stylesheet">

    <title>Programming | Target Turner Range Control</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Nodes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <h5 class="display-3">Programme Control</h5>
                <p>Enables you to manually control your target turners</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                Target: <select id="node" name="node" class="form-control">
                                    <option value="1">Target 1</option>
                                    <option value="2">Target 2</option>
                                    <option value="3">Target 3</option>
                                    <option value="4">Target 4</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                Duration (secs): <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio1" value="2">
  <label class="form-check-label" for="inlineRadio1">2</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio2" value="3">
  <label class="form-check-label" for="inlineRadio2">3</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio3" value="4">
  <label class="form-check-label" for="inlineRadio3">4</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio4" value="5">
  <label class="form-check-label" for="inlineRadio4">5</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio5" value="6">
  <label class="form-check-label" for="inlineRadio5">6</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio6" value="7">
  <label class="form-check-label" for="inlineRadio6">7</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio7" value="8">
  <label class="form-check-label" for="inlineRadio7">8</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio8" value="9">
  <label class="form-check-label" for="inlineRadio8">9</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio9" value="10">
  <label class="form-check-label" for="inlineRadio9">10</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="time" id="inlineRadio10" value="0" checked>
  <label class="form-check-label" for="inlineRadio10">Random</label>
</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            Direction: <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="direction" id="direction1" value="1">
                                        <label class="form-check-label" for="direction1">Anti-Clockwise</label>
                                       </div>
                                       <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="direction" id="direction2" value="2">
                                        <label class="form-check-label" for="direction2">Clockwise</label>
                                       </div>
                                       <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="direction" id="direction3" value="0" checked>
                                        <label class="form-check-label" for="direction3">Random</label>
                                       </div>
                            </div>
                            <div class="col-md-6">
                                Duration:
                                <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="duration" id="duration1" value="10" checked>
                                        <label class="form-check-label" for="duration1">10 Turns</label>
                                       </div>
                                       <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="duration" id="duration2" value="20">
                                        <label class="form-check-label" for="duration2">20 Turns</label>
                                       </div>
                                       <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="duration" id="duration3" value="30">
                                        <label class="form-check-label" for="duration3">30 Turns</label>
                                       </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            Delay (secs): <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay1" value="5">
  <label class="form-check-label" for="delay1">5</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay2" value="6">
  <label class="form-check-label" for="delay2">6</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay3" value="7">
  <label class="form-check-label" for="delay3">7</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay4" value="8">
  <label class="form-check-label" for="delay4">8</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay5" value="9">
  <label class="form-check-label" for="delay5">9</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay6" value="10">
  <label class="form-check-label" for="delay6">10</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay7" value="11">
  <label class="form-check-label" for="delay7">11</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay8" value="12">
  <label class="form-check-label" for="delay8">12</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay9" value="13">
  <label class="form-check-label" for="delay9">13</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay10" value="14">
  <label class="form-check-label" for="delay10">14</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay11" value="15">
  <label class="form-check-label" for="delay11">15</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay12" value="16">
  <label class="form-check-label" for="delay12">16</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay13" value="17">
  <label class="form-check-label" for="delay13">17</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay14" value="18">
  <label class="form-check-label" for="delay14">18</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay15" value="19">
  <label class="form-check-label" for="delay15">19</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay16" value="20">
  <label class="form-check-label" for="delay16">20</label>
                            </div>
                            <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="delay" id="delay17" value="0" checked>
  <label class="form-check-label" for="delay17">Random</label>
                            </div>
                        </div>
                        <div clas="row">
                            <div class="col-md-6">
                            <input type="submit" name="submit" class="btn btn-primary" value="Run"/>
                            </div>
                            <div class="col-md-6">
                                &nbsp;
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>

      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <textarea type="text" rows="10" id="results" value="$_SESSION('output')"></textarea>
        </div>

        <hr>

      </div> <!-- /container -->

    </main>

    <footer class="container">
      <p>&copy; Daz Co. Networks 2020</p>
    </footer>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="js/popper.min.js" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" crossorigin="anonymous"></script>

    </body>
</html>

